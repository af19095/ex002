import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;

    private JButton karaageButton;
    private JButton yakisobaButton;
    private JButton ramenButton1;
    private JTextPane orderedItems;

    private JButton checkOutButton;
    private JLabel Label001;
    private JLabel Label002;
    private JLabel Label003;
    private JLabel Total;
    int total = 0;


    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order "+food+"?", "Order Confirmation", JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+food+"! It will be served as soon as possible.");
                String currentText =orderedItems.getText();
                    orderedItems.setText(currentText+food+"\n");
                        total+=100;
                            Total.setText(total+"yen");
        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
            frame.setContentPane(new FoodGUI().root);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                        frame.setVisible(true);
    }

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");

            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                  order("Udon");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });


        ramenButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout01= JOptionPane.showConfirmDialog(null, "Would you like to checkout?","Check Out",JOptionPane.YES_NO_OPTION);
                if(checkout01==0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+total+" yen.");
                        orderedItems.setText("");
                            Total.setText("");
                }
            }
        });
    }
}
